/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import java.util.ArrayList;
import olio.ohjelmointi.viikko.pkg9.FXMLDocumentController;

/**
 *
 * @author Jönnsson
 */
public class Mainclass {
    
    private Document doc;
    private ArrayList<String> esitysList = new ArrayList<>();
    private ArrayList<String> haettavaEsitys = new ArrayList<>();
    private ArrayList<String> haettavaEsitysTeattereissa = new ArrayList<>();

    public ArrayList<String> getHaettavaEsitys() {
        return haettavaEsitys;
    }

    public ArrayList<String> getHaettavaEsitysTeattereissa() {
        return haettavaEsitysTeattereissa;
    }
    static private Mainclass wt = null;

    public ArrayList<String> getesitysList() {
        return esitysList;
    }
    
    public Mainclass(){
    }
    
    static public Mainclass getInstance() {
        if(wt == null){
            wt = new Mainclass();
        }
        return wt;
    }
 
    public void WebKäsittely(String content,String tagName, String valueName, String startTime, String endTime) throws MalformedURLException, IOException, ParseException {

        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            
            doc.getDocumentElement().normalize();
            
            if( "TheatreArea".equals(tagName)){
                parseTeatterit(tagName, valueName);
            }
            else if("Show".equals(tagName)){
                parseEsitykset(tagName, valueName, startTime, endTime);
            }
            else if("Seek".equals(tagName)){
                seekMovie(valueName);
            }
            else if("SeekforTheatre".equals(tagName)){
                seekMovieFromTheaters(valueName);
            }
            
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(Mainclass.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void parseTeatterit(String tagName, String valueName){
        NodeList nodes = doc.getElementsByTagName(tagName);
        
        for(int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            //valuename on nyt teatterin nimi
            if( "TheatreArea".equals(tagName)){
                Teatteri teatteri = new Teatteri(getValue("ID",e),getValue(valueName,e));
            }
        }
        }
    
    public String getValue(String tag, Element e) {
        return ((Element)e.getElementsByTagName(tag).item(0)).getTextContent(); 
    }
    
    public void parseEsitykset(String tagName, String valueName,String startTime, String endTime) throws ParseException{
        NodeList nodes = doc.getElementsByTagName(tagName);
        SimpleDateFormat parser = new SimpleDateFormat("HH:mm:ss");
        java.util.Date startTimeTemp = parser.parse(startTime);
        java.util.Date endTimeTemp = parser.parse(endTime);
        for(int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            //valuename on nyt teatterin nimi
            java.util.Date showStartTime = parser.parse(getValue("dttmShowStart",e).split("T")[1]);
            java.util.Date showEndTime = parser.parse(getValue("dttmShowEnd",e).split("T")[1]);

            if( "Show".equals(tagName) && showStartTime.after(startTimeTemp) && endTimeTemp.after(showEndTime) ){
                esitysList.add("Nimi: " + getValue("Title",e));
                esitysList.add("Kesto [min]: " + getValue("LengthInMinutes",e));
                esitysList.add("Alkamiskellonaika: " + getValue("dttmShowStart",e).split("T")[1]);
                esitysList.add("Teatteri ja näytössali: " + getValue("TheatreAndAuditorium",e) + "\n\n-------------------------------------------------------------------------------\n");

            }
        }
        
    }
    
    
    
    public String urlKäsittely(URL url) throws IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        String content = "";
        String line = "";
        
        while((line = br.readLine()) != null) {
            content += line + "\n";
        }
        return content;
    }
    
    
    public void etsiElokuvaKäsittely(String nimi) throws MalformedURLException, IOException, ParseException{
        haettavaEsitys.clear();
        for(Teatteri t:Teatteri.getTeatterilista()){
            if(haettavaEsitys.isEmpty() == true){
                URL url2 = new URL("http://www.finnkino.fi/xml/Schedule/?area="+ t.getId() +"&dt=");
                String content = urlKäsittely(url2);
                WebKäsittely(content, "Seek",nimi,"","");
            }
            else{
                break;
            }
        }
        if(haettavaEsitys.isEmpty() == true){
            haettavaEsitys.add("Haettavaa esitystä ei löydy!!!\n\n");
        }
    }
    
    public void seekMovie(String name){
        NodeList nodes = doc.getElementsByTagName("Show");
        for(int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            if(name.equals(getValue("Title",e))){
                haettavaEsitys.add("HAETTAVAN ESITYKSEN TIEDOT\n\n");
                haettavaEsitys.add(getValue("Title",e));
                haettavaEsitys.add("Kesto [min]: " + getValue("LengthInMinutes",e));
                haettavaEsitys.add("Genre: " + getValue("Genres",e));
                haettavaEsitys.add("Ikäraja: " + getValue("Rating",e));
                haettavaEsitys.add("\n\n-------------------------------------------------------------------------------\n");
            }
            i = nodes.getLength();
        }
    }
    public void etsiElokuvaaTeattereista(String nimi) throws MalformedURLException, IOException, ParseException{
        haettavaEsitysTeattereissa.clear();
        for(Teatteri t:Teatteri.getTeatterilista()){
            URL url2 = new URL("http://www.finnkino.fi/xml/Schedule/?area="+ t.getId() +"&dt=");
            String content = urlKäsittely(url2);
            WebKäsittely(content, "SeekforTheatre",nimi,"","");
        }
    }
    
    public void seekMovieFromTheaters(String nimi){
        NodeList nodes = doc.getElementsByTagName("Show");
        for(int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            if(nimi.equals(getValue("Title",e))){
                haettavaEsitysTeattereissa.add(getValue("TheatreAndAuditorium",e));
                haettavaEsitysTeattereissa.add(getValue("dttmShowStart",e));
                haettavaEsitysTeattereissa.add("\n");
            }
        }
    }
}
    

