/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import java.util.ArrayList;
import static oracle.jrockit.jfr.events.Bits.intValue;



/**
 *
 * @author Jönnsson
 */
public class Teatteri {
    String paikkakunta;

    public String getPaikkakunta() {
        return paikkakunta;
    }
    Integer id;

    public Integer getId() {
        return id;
    }
    
    static ArrayList<Teatteri> teatterilista = new ArrayList<>();

    public static ArrayList<Teatteri> getTeatterilista() {
        return teatterilista;
    }
    
    
    public Teatteri(String x, String y){
        Double temp = Double.valueOf(x);
        id = intValue(temp);
        paikkakunta = y;
        teatterilista.add(this);
    }
    @Override
   public String toString(){
       String temp;
       temp = paikkakunta;
       return temp;
   }
    
    
}
