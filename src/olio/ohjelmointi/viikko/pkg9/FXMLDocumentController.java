/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio.ohjelmointi.viikko.pkg9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import web.Teatteri;
import web.Mainclass;
import java.util.HashMap;

/**
 *
 * @author Jönnsson
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private Button seekMovie;
    @FXML
    private ComboBox<Teatteri> theatreChooser;
    @FXML
    private TextField filmDay;
    @FXML
    private TextField endingTime;
    @FXML
    private TextField startingTime;
    @FXML
    private TextField movieName;
    @FXML
    private TextArea textBox;
    @FXML
    private Button listMovies;
    

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            url = new URL("http://finnkino.fi/xml/TheatreAreas/");
        } catch (MalformedURLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        String content = "";
        Mainclass w = new Mainclass();
        try {
            content = w.urlKäsittely(url);
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        String tagName = "TheatreArea";
        String valueName = "Name";
        try {
            w.WebKäsittely(content, tagName, valueName,"","");
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        for(Teatteri teatteri:Teatteri.getTeatterilista()){
            theatreChooser.getItems().add(teatteri);
        }
    }

    @FXML 
    public void HaeAikataulut() throws MalformedURLException, IOException, ParseException{
            try{
                URL url2 = new URL("http://www.finnkino.fi/xml/Schedule/?area="+ theatreChooser.getValue().getId() +"&dt=" + filmDay.getText());
                String content2 = "";
                String startTime;
                String endTime;
                Mainclass w2 = new Mainclass();
                content2 = w2.urlKäsittely(url2);

                if(startingTime.getText().trim().length()<1){
                    startTime = "00:00:01";
                }
                else{
                    startTime = startingTime.getText();
                }
                if(endingTime.getText().trim().length()<1){
                    endTime = "23:59:59";
                }
                else{
                    endTime = endingTime.getText();
                } 
                try{
                    w2.WebKäsittely(content2, "Show",filmDay.getText(),startTime,endTime);

                    ArrayList<String> list = w2.getesitysList();
                    int i = 0;
                    textBox.clear();
                    for(String temp:list){
                        textBox.appendText(list.get(i) + "\n");
                        i++;
                    }
                } catch (IOException | ParseException ex) {
                    textBox.appendText("Tarkista päivämäärä ja kellonajat TÖHÖ!\n");
                }
            } catch (Exception ex){
                textBox.appendText("Tarkista päivämäärä ja kellonajat TÖHÖ!\n");
            }
            
            
    }
    
    @FXML
    public void HaeNimellä() throws IOException, MalformedURLException, ParseException{
        textBox.clear();
        Mainclass w = new Mainclass();
        w.etsiElokuvaKäsittely(movieName.getText());
        ArrayList<String> temp = new ArrayList<>();
        temp = w.getHaettavaEsitys();
        for(String s:temp){
            textBox.appendText(s + "\n");
        }
        if(w.getHaettavaEsitys().isEmpty() == false){
            w.etsiElokuvaaTeattereista(w.getHaettavaEsitys().get(1));
            w.getHaettavaEsitysTeattereissa().forEach((s2) -> {
                textBox.appendText(s2 + "\n");
            });
        }
    }
}
